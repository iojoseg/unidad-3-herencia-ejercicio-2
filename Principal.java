public class Principal
{
     public static void main(String[] args) {

        Perro perro = new Perro("Maky","Croquetas",10,"Chihuahua");
        Gato gato = new Gato("Michi","Whiskas",8,"Siames");
        Caballo caballo = new Caballo("Jhonny","Heno",21,"Cuarto milla");

        //-->Nos muestra los detalles del objeto
        perro.mostrar();
        System.out.println("--------------------------------------------------");
        gato.mostrar();
        System.out.println("--------------------------------------------------");
        caballo.mostrar();

    }   
}
