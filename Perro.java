
public class Perro extends Animal
{
    
    
    public Perro(String nombre, String tipo_alimentacion, int edad, String raza){
    super(nombre,tipo_alimentacion,edad,raza);
    }
    
    
    public void mostrar(){
        System.out.println(getNombre() + " - "+getTipo_alimentacion()+" - "+getEdad()+" - "+getRaza());
    }
    
}