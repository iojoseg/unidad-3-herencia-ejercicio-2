public class Animal
{
    private String nombre; 
    private String tipo_alimentacion;
    private int edad;
    private String raza;
    
    public Animal(String nombre, String tipo_alimentacion, int edad, String raza){
    this.nombre = nombre;
    this.tipo_alimentacion = tipo_alimentacion;
    this.edad = edad;
    this.raza = raza;
    }
    
    public String getNombre(){
    return nombre;
    }
    
    public void setNombre(String nombre){
    this.nombre = nombre;
    }
    
    public String getTipo_alimentacion(){
    return tipo_alimentacion;
    }
    
    public void setTipo_alimentacion(String tipo_alimentacion){
    this.tipo_alimentacion = tipo_alimentacion;
    }
    
    public int getEdad(){
    return edad;
    }
    
    public void setEdad(int edad){
    this.edad = edad;
    }
    
    public String getRaza(){
    return raza;
    }
    
    public void setRaza(String raza) {
        this.raza = raza;
    }

    
}
